//
//  Reader.swift
//  Syzygies
//
//  Created by Maximilian Denninger on 26.10.15.
//  Copyright © 2015 Maximilian Denninger. All rights reserved.
//

import Foundation


class Reader {
    var m_lines : [String];
    init(filePath : String){
        var text = "";
        do {
            text = try String(contentsOfFile: filePath, encoding: NSUTF8StringEncoding);
        }catch let error as NSError {
            print("Reading of the file failed with error: \(error)");
            m_lines = [];
            return;
        }
        //println("text: \(text)");
        m_lines = text.componentsSeparatedByString("\r\n");
        m_lines.removeLast(); // remove empty
    }
}