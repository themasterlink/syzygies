//
//  SortedWords.swift
//  Syzygies
//
//  Created by Maximilian Denninger on 26.10.15.
//  Copyright © 2015 Maximilian Denninger. All rights reserved.
//

import Foundation

extension String {
    func getLastChars() -> (nextToLast: Character, last : Character) {
        // will fail for strings under length 2!
        return (self[self.endIndex.predecessor().predecessor()], self[self.endIndex.predecessor()]);
    }
}

class Leaf {
    let m_head : Character;
    var m_words : [Word] = [];
    init(head : Character){
        m_head = head;
    }
}

class Element {
    let m_head : Character;
    var m_leafs : [Leaf];
    init(head : Character){
        m_head = head;
        m_leafs = [];
    }
    
    func appendWords( words : Words, startValue : Int, frontPart : Bool) -> Int {
        if(frontPart){
            let wordsCount = words.count;
            for var i = startValue; i < wordsCount; ++i {
                let word = words[i];
                let firstChar = word.s.characters.first!;
                if(firstChar == m_head && word.s.lengthOfBytesUsingEncoding(word.s.fastestEncoding) > 1){
                    let secondChar = word.s[word.s.startIndex.advancedBy(1)];
                    var found = false;
                    for leaf in m_leafs {
                        if(leaf.m_head == secondChar){
                            found = true;
                            leaf.m_words.append(word);
                            break;
                        }
                    }
                    if(!found){
                        let newLeaf = Leaf(head: secondChar);
                        newLeaf.m_words.append(word);
                        m_leafs.append(newLeaf);
                    }
                }else{
                    return i + 1;
                }
            }
            return wordsCount;
        }else{
            
        }
        return startValue;
    }
}

class Words : CustomStringConvertible {
    var m_words : [Word] = [];
    
    var count : Int = 0;
    
    init(list : [String]){
        count = list.count;
        var i = 0;
        m_words = [Word](count: count, repeatedValue: Word());
        for name in list {
            if(name.lengthOfBytesUsingEncoding(name.fastestEncoding) > 1){
                m_words[i++] = Word(s: name);
            }
        }
    }
    
    var description : String {
        var ret = "";
        for word in m_words {
            ret += "\(word)";
        }
        return ret;
    }
    
    subscript(index : Int) -> Word {
        get {
            return m_words[index];
        }
    }
}

class Word : CustomStringConvertible {
    var s : String = "";
    init(){
    }
    init(s : String){
        self.s = s;
    }
    
    var description : String {
        get{
            return s;
        }
    }
}

class SortedWords{
    
    var m_elements : [Element] = [];
    
    var m_words : Words;
    
    init(words : Words, frontPart : Bool){
        let wordCount = words.count;
        m_words = words;
        if(frontPart){
            var startValue = 0;
            while(startValue < wordCount){
                let firstChar = m_words[startValue].s.characters.first!;
                if(firstChar != m_elements.last?.m_head){
                    m_elements.append(Element(head: firstChar));
                }
                print("\(startValue): \(firstChar) \(m_words[startValue])");
                startValue = (m_elements.last?.appendWords(m_words, startValue: startValue, frontPart: true))!;
                
            }
        }else{
            
        }
    }

    func getWords(firstChar : Character, _ secondCharacter : Character) -> [Word] {
        for ele in m_elements{
            if(ele.m_head == firstChar){
                print("Ele \(ele.m_head)");
                for leaf in ele.m_leafs {
                    print("Leaf: \(leaf.m_head)");
                    if(leaf.m_head == secondCharacter){
                        return leaf.m_words;
                    }
                }
                break;
            }
        }
        return [];
    }
}
