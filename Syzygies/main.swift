//
//  main.swift
//  Syzygies
//
//  Created by Maximilian Denninger on 26.10.15.
//  Copyright © 2015 Maximilian Denninger. All rights reserved.
//

import Foundation

var reader = Reader(filePath: "/Users/Max/Documents/XCode_Projects/Bitbucket/Syzygies/Syzygies/words.txt");

var words = Words(list: reader.m_lines);
var sortedWords = SortedWords(words: words, frontPart: true);

print("\(sortedWords.getWords("a","b"))");
//print("\(reader.m_lines)");
